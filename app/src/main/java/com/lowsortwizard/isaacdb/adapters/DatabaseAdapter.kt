package com.lowsortwizard.isaacdb.adapters

import android.content.Context
import android.graphics.BitmapFactory
import com.lowsortwizard.isaacdb.model.ActivatedItem
import com.lowsortwizard.isaacdb.model.Item
import com.lowsortwizard.isaacdb.ui.artifacts.ActiveArtifactsViewModel

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper

class DatabaseAdapter(context: Context) :
    SQLiteAssetHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    fun getItems(tableName: String): List<Item> {
        val database = writableDatabase
        val cursor = database.rawQuery("SELECT * FROM $tableName", null)
        val items = mutableListOf<Item>()

        if (tableName == ActiveArtifactsViewModel.TABLE_NAME) {
            cursor.moveToFirst()
            do {
                items.add(
                    ActivatedItem(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getInt(2),
                        BitmapFactory.decodeByteArray(
                            cursor.getBlob(3),
                            0,
                            cursor.getBlob(3).size
                        )
                    )
                )
            } while (cursor.moveToNext())
        } else {
            cursor.moveToFirst()
            do {
                items.add(
                    Item(
                        cursor.getString(0),
                        cursor.getString(1),
                        BitmapFactory.decodeByteArray(
                            cursor.getBlob(2),
                            0,
                            cursor.getBlob(2).size
                        )
                    )
                )
            } while (cursor.moveToNext())
        }

        cursor.close()
        database.close()
        return items
    }

    companion object {
        private const val DATABASE_NAME = "items.db"
        private const val DATABASE_VERSION = 1
    }
}
