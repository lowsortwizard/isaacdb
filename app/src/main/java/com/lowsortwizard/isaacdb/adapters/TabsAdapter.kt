package com.lowsortwizard.isaacdb.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.lowsortwizard.isaacdb.ui.artifacts.ArtifactsFragment

class TabsAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ArtifactsFragment.ActiveArtifactsTab()
            else -> ArtifactsFragment.PassiveArtifactsTab()
        }
    }

    override fun getCount(): Int {
        return TABS_QUANTITY
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> TAB_ACTIVE_ARTIFACTS
            else -> TAB_PASSIVE_ARTIFACTS
        }
    }

    companion object {
        const val TABS_QUANTITY = 2
        const val TAB_ACTIVE_ARTIFACTS = "ACTIVE"
        const val TAB_PASSIVE_ARTIFACTS = "PASSIVE"
    }
}
