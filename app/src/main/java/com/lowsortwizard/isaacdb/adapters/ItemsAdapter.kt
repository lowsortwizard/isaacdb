package com.lowsortwizard.isaacdb.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.lowsortwizard.isaacdb.R
import com.lowsortwizard.isaacdb.model.ActivatedItem
import com.lowsortwizard.isaacdb.model.Item
import kotlinx.android.synthetic.main.list_item.view.*

class ItemsAdapter(private val items: List<Item>) :
    RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        return ItemsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Item) {
            itemView.item_icon.setImageBitmap(item.icon)
            itemView.item_name.text = item.name
            itemView.item_description.text = item.description
            if (item is ActivatedItem) itemView.item_charge.text = item.charge.toString()
        }
    }
}
