package com.lowsortwizard.isaacdb.ui.cards

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.lowsortwizard.isaacdb.adapters.DatabaseAdapter

class CardsViewModel(application: Application) : AndroidViewModel(application) {

    private val adapter = DatabaseAdapter(getApplication())
    val originalCards = adapter.getItems(TABLE_NAME)

    companion object {
        private const val TABLE_NAME = "cards"
    }
}