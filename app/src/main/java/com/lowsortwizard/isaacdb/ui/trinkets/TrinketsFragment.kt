package com.lowsortwizard.isaacdb.ui.trinkets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lowsortwizard.isaacdb.adapters.ItemsAdapter
import com.lowsortwizard.isaacdb.R

class TrinketsFragment : Fragment() {

    private lateinit var trinketsViewModel: TrinketsViewModel
    private lateinit var trinkets: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        trinketsViewModel = ViewModelProviders.of(this).get(TrinketsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_trinkets, container, false)
        trinkets = root.findViewById(R.id.trinkets_recyclerview)
        trinkets.layoutManager = LinearLayoutManager(context)
        trinkets.setHasFixedSize(true)
        trinkets.adapter = ItemsAdapter(trinketsViewModel.originalTrinkets)
        return root
    }
}