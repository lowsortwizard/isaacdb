package com.lowsortwizard.isaacdb.ui.trinkets

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.lowsortwizard.isaacdb.adapters.DatabaseAdapter

class TrinketsViewModel(application: Application) : AndroidViewModel(application) {

    private val adapter = DatabaseAdapter(getApplication())
    val originalTrinkets = adapter.getItems(TABLE_NAME)

    companion object {
        private const val TABLE_NAME = "trinkets"
    }
}