package com.lowsortwizard.isaacdb.ui.artifacts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.lowsortwizard.isaacdb.R
import com.lowsortwizard.isaacdb.adapters.ItemsAdapter
import com.lowsortwizard.isaacdb.adapters.TabsAdapter


class ArtifactsFragment : Fragment() {

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_artifacts, container, false)
        viewPager = root.findViewById(R.id.artifacts_viewpager)
        tabLayout = root.findViewById(R.id.artifacts_tabs)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val adapter = TabsAdapter(activity!!.supportFragmentManager)
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }

    class ActiveArtifactsTab : Fragment() {

        private lateinit var activeArtifactsViewModel: ActiveArtifactsViewModel
        private lateinit var activeArtifacts: RecyclerView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            activeArtifactsViewModel =
                ViewModelProviders.of(this).get(ActiveArtifactsViewModel::class.java)
            val root = inflater.inflate(R.layout.tab_active_artifacts, container, false)
            activeArtifacts = root.findViewById(R.id.active_artifacts_recyclerview)
            activeArtifacts.layoutManager = LinearLayoutManager(context)
            activeArtifacts.setHasFixedSize(true)
            activeArtifacts.adapter = ItemsAdapter(activeArtifactsViewModel.originalActiveArtifacts)
            return root
        }
    }

    class PassiveArtifactsTab : Fragment() {

        private lateinit var passiveArtifactsViewModel: PassiveArtifactsViewModel
        private lateinit var passiveArtifacts: RecyclerView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            passiveArtifactsViewModel =
                ViewModelProviders.of(this).get(PassiveArtifactsViewModel::class.java)
            val root = inflater.inflate(R.layout.tab_passive_artifacts, container, false)
            passiveArtifacts = root.findViewById(R.id.passive_artifacts_recyclerview)
            passiveArtifacts.layoutManager = LinearLayoutManager(context)
            passiveArtifacts.setHasFixedSize(true)
            passiveArtifacts.adapter =
                ItemsAdapter(passiveArtifactsViewModel.originalPassiveArtifacts)
            return root
        }
    }

}