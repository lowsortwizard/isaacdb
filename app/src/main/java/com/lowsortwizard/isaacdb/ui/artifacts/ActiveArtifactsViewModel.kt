package com.lowsortwizard.isaacdb.ui.artifacts

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.lowsortwizard.isaacdb.adapters.DatabaseAdapter

class ActiveArtifactsViewModel(application: Application) : AndroidViewModel(application) {

    private val adapter = DatabaseAdapter(getApplication())
    val originalActiveArtifacts = adapter.getItems(TABLE_NAME)

    companion object {
        const val TABLE_NAME = "active_artifacts"
    }

}