package com.lowsortwizard.isaacdb.ui.artifacts

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.lowsortwizard.isaacdb.adapters.DatabaseAdapter

class PassiveArtifactsViewModel(application: Application) : AndroidViewModel(application) {

    private val adapter = DatabaseAdapter(getApplication())
    val originalPassiveArtifacts = adapter.getItems(TABLE_NAME)

    companion object {
        private const val TABLE_NAME = "passive_artifacts"
    }
}