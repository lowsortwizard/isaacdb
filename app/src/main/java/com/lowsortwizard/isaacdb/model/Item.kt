package com.lowsortwizard.isaacdb.model

import android.graphics.Bitmap

open class Item(val name: String, val description: String, val icon: Bitmap)
