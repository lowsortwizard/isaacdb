package com.lowsortwizard.isaacdb.model

import android.graphics.Bitmap

class ActivatedItem(name: String, description: String, val charge: Int, icon: Bitmap) :
    Item(name, description, icon)
